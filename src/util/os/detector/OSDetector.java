package util.os.detector; 

import java.awt.Desktop;
import java.io.File;

public class OSDetector {
	private static boolean isWindows = false;
	private static boolean isLinux = false;
	private static boolean isMac = false;

	static {
		String os = System.getProperty("os.name").toLowerCase();
		isWindows = os.contains("win");
		isLinux = os.contains("nux") || os.contains("nix");
		isMac = os.contains("mac");
	}

	public static boolean isWindows() {
		return isWindows;
	}

	public static boolean isLinux() {
		return isLinux;
	}

	public static boolean isMac() {
		return isMac;
	};

	public static String getTempDir() {
		String tempdir = System.getProperty("java.io.tmpdir");
		if (!(tempdir.endsWith("/") || tempdir.endsWith("\\")))
			tempdir = tempdir + System.getProperty("file.separator");
		return tempdir;
	}

	public static boolean openFile(File FileObj) {
		try {
			if (isWindows) {
				Process p = Runtime.getRuntime().exec(
						"rundll32 url.dll,FileProtocolHandler "
								+ FileObj.getAbsolutePath());
				p.waitFor();
				return true;
			} else if (isLinux || isMac) {
				Process p = Runtime.getRuntime().exec(
						"/usr/bin/open " + FileObj.getAbsolutePath());
				p.waitFor();
				return true;
			} else {
				// Unknown OS, try with desktop
				if (Desktop.isDesktopSupported()) {
					Desktop.getDesktop().open(FileObj);
					return true;
				} else {
					return false;
				}
			}
		} catch (Exception ex) {
			return false;
		}
	}
}
